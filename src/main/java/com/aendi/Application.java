package com.aendi;

import com.aendi.dao.MessageDao;
import com.aendi.dao.MessageEntity;
import org.flywaydb.core.Flyway;

import java.util.List;

public class Application {
    public static void main(String[] args) throws Exception {
        String url = "jdbc:postgresql://localhost:5432/jdbc-example";
        String user = "postgres";
        String password = "";
        Flyway flyway = Flyway.configure().dataSource(url, user, password).load();
        flyway.migrate();
        MessageDao messageDao = new MessageDao();
        MessageEntity messageEntity1 = new MessageEntity();
        MessageEntity messageEntity2 = new MessageEntity();
        MessageEntity messageEntity3 = new MessageEntity();
        MessageEntity messageEntity4 = new MessageEntity();
        messageEntity1.setContent("con tent");
        messageEntity2.setContent("Crub");
        messageEntity3.setContent("whoppa");
        messageEntity4.setContent("live leave");
        messageEntity1 = messageDao.addMessage(messageEntity1);
        messageDao.addMessage(messageEntity2);
        messageDao.addMessage(messageEntity3);
        messageEntity1.setContent("con tent ++");
        messageDao.updateMessage(messageEntity1);
        messageDao.addMessage(messageEntity4);
        MessageEntity byId = messageDao.getById(messageEntity1.getId());
        List<MessageEntity> byWord = messageDao.getByWord("con");
        messageDao.deleteMessage(2L);
        List<MessageEntity> allMessages = messageDao.getAllMessages();
    }
}
