package com.aendi.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCProvider {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/jdbc-example";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "";

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    }
}
