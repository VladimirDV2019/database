package com.aendi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class MessageDao {
    public MessageEntity getById(Long id) throws Exception {
        try (Connection connection = JDBCProvider.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement("select * from \"message\" where id=?")) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createMessage(resultSet);
            }
        }
        throw new Exception("Message with id:" + id + "not found");
    }

    public List<MessageEntity> getByWord(String word) throws Exception {
        try (Connection connection = JDBCProvider.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement("select * from \"message\" where content like '%' || ? || '%'")) {
            List<MessageEntity> messages = new ArrayList<>();
            preparedStatement.setString(1, word);
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                messages.add(createMessage(resultSet));
            }
            if (messages.isEmpty())
                throw new Exception("Message which contains:" + word + "not found");
            return messages;
        }
    }

    public MessageEntity addMessage(MessageEntity message) throws Exception {
        try (Connection connection = JDBCProvider.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement("insert into \"message\"(content) values(?)", RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, message.getContent());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                final long messageId = resultSet.getLong(1);
                message.setContent(message.getContent());
                message.setId(messageId);
                return message;
            }
            throw new Exception("Message cannot be added");
        }
    }

    public void updateMessage(MessageEntity message) throws Exception {
        try (Connection connection = JDBCProvider.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement("update \"message\" set content=? where id=?")) {
            preparedStatement.setString(1, message.getContent());
            preparedStatement.setLong(2, message.getId());
            int countOfUpdatedRows = preparedStatement.executeUpdate();
            if (countOfUpdatedRows == 0) {
                throw new Exception("Can't update message. Message " + message.getId() + " not found");
            }
        }
    }

    public void deleteMessage(Long id) throws Exception {
        try (Connection connection = JDBCProvider.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement("delete from \"message\" where id=?")) {
            preparedStatement.setLong(1, id);
            int deletedRows = preparedStatement.executeUpdate();
            if (deletedRows == 0) {
                throw new Exception("Can't delete message. Message " + id + " not found");
            }
        }
    }

    public List<MessageEntity> getAllMessages() throws Exception {
        try (Connection connection = JDBCProvider.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement("select * from \"message\"")) {
            List<MessageEntity> messages = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                MessageEntity message = createMessage(resultSet);
                messages.add(message);
            }
            if (messages.isEmpty()) {
                throw new Exception("There are no messages");
            }
            return messages;
        }
    }

    private MessageEntity createMessage(ResultSet resultSet) throws SQLException {
        MessageEntity message = new MessageEntity();
        message.setId(resultSet.getLong("id"));
        message.setContent(resultSet.getString("content"));
        return message;
    }
}
